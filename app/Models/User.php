<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use HasRoles;
use Spatie\Permission\Traits\HasRoles as TraitsHasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,TraitsHasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'token',
        'social_id',
        'doctor_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'social_id',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function parent()
    {
        return $this->hasOne(User::class, 'id', 'doctor_id');
    }
    public function secretaries()
    {
        return $this->hasMany(User::class, 'doctor_id', 'id');
    }
}
