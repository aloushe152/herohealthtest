<?php

use App\Http\Resources\MediaResource;
use App\Models\Media;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\PersonalAccessToken;

if (!function_exists('to_user')) {
    function to_user($user): ?User
    {
        return $user;
    }
}

if (!function_exists('to_token')) {
    function to_token($token): ?PersonalAccessToken
    {
        return $token;
    }
}


