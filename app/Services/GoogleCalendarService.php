<?php
namespace App\Services;

use Google\Client;
use Illuminate\Support\Facades\Auth;

class GoogleCalendarService
{
    public static function getService()
    {
        $client = new Client();
        $client->setClientId(config('services.google.client_id'));
        $client->setClientSecret(config('services.google.client_secret'));
        $client->setRedirectUri(config('services.google.redirect_uri'));

        $client->setApprovalPrompt('force');
        $client->addScope('https://www.googleapis.com/auth/calendar.events');
        $client->setScopes([
            \Google\Service\Oauth2::USERINFO_PROFILE,
            \Google\Service\Oauth2::USERINFO_EMAIL,
            \Google\Service\Oauth2::OPENID,
            \Google\Service\Calendar::CALENDAR,
            \Google\Service\Calendar::CALENDAR_EVENTS,
        ]);
        $client->setIncludeGrantedScopes(true);
        $client->refreshToken(to_user(Auth::user())->token);

        // Check if the access token is expired or about to expire
        if ($client->isAccessTokenExpired()) {
            // Refresh the access token
            $client->fetchAccessTokenWithRefreshToken(["refresh_token" => to_user(Auth::user())->token]);

            // Get the updated access token
            $accessToken = $client->getAccessToken();

            // Save the updated access token in the user model
            $client->setAccessToken($accessToken);
        }
        $service = new \Google\Service\Calendar($client);
        return $service;
    }
}
