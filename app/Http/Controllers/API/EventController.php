<?php

namespace  App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Events\StoreEventRequest;
use App\Http\Requests\Events\UpdateEventRequest;
use App\Services\GoogleCalendarService as ServicesGoogleCalendarService;
use Google\Service\Calendar\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        if (to_user(Auth::user())->hasPermissionTo('event-index')) {
            $service = ServicesGoogleCalendarService::getService();

            $events = $service->events->listEvents('primary');

            return  response()->json($events->getItems());
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEventRequest $request)
    {
        if (to_user(Auth::user())->hasPermissionTo('event-store')) {

            $service = ServicesGoogleCalendarService::getService();
            $event = new Event([
                'summary' =>  $request->summary,
                'description' => $request->description,
                'start' => [
                    'dateTime' =>  $request->start, //'2023-09-28T10:00:00', // Specify the start date and time
                    'timeZone' => date_default_timezone_get(), // Replace with the desired time zone
                ],
                'end' => [
                    'dateTime' => $request->end, // Specify the end date and time
                    'timeZone' => date_default_timezone_get(), // Replace with the desired time zone
                ],
            ]);
            $calendarId = 'primary'; // Use 'primary' for the user's primary calendar
            $createdEvent = $service->events->insert($calendarId, $event);
            return  response()->json($createdEvent);
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (to_user(Auth::user())->hasPermissionTo('event-index')) {
            $service = ServicesGoogleCalendarService::getService();
            $event = $service->events->get('primary', $id);

            return  response()->json($event);
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEventRequest $request, string $id)
    {
        if (to_user(Auth::user())->hasPermissionTo('event-update')) {

            $service = ServicesGoogleCalendarService::getService();
            $event = $service->events->get('primary', $id);

            $end = $event->getEnd();
            $start = $event->getStart();
            if ($request->end) {
                $end = new \Google\Service\Calendar\EventDateTime();
                $end->setDateTime($request->end);
                $end->setTimeZone(date_default_timezone_get());
            }
            if ($request->start) {
                $start = new \Google\Service\Calendar\EventDateTime();
                $start->setDateTime($request->start);
                $start->setTimeZone(date_default_timezone_get());
            }
            $event->setSummary($request->summary);
            $event->setDescription($request->description);
            $event->setStart($start);
            $event->setEnd($end);

            $updated_event = $service->events->update('primary', $id, $event);
            return  response()->json($updated_event);
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (to_user(Auth::user())->hasPermissionTo('event-destroy')) {
            $service = ServicesGoogleCalendarService::getService();
            try {
                $service->events->delete('primary', $id);
            } catch (\Throwable $th) {
                return  response()->json('Not Found', 404);
            }
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }
}
