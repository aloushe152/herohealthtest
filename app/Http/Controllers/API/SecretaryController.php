<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SecretaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (to_user(Auth::user())->hasPermissionTo('secretary-index')) {
            $secretaries =to_user(Auth::user())->secretaries()->get();
            return response()->json($secretaries);
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {

        if (to_user(Auth::user())->hasPermissionTo('secretary-store')) {
            $secretary = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'token' => '',
                'password' => $request->password,
                'social_id' => '',
                'doctor_id' => to_user(Auth::user())->id
            ]);
            $secretary->assignRole('secretary');
            return  response()->json($secretary, 200);
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (to_user(Auth::user())->hasPermissionTo('secretary-index')) {
            $secretary =to_user(Auth::user())->secretaries()->find($id);
            if ($secretary) {
                return response()->json($secretary, 200,);
            } else {
                return response()->json('Not Found', 404);
            }
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $id)
    {

        if (to_user(Auth::user())->hasPermissionTo('secretary-update')) {
            $secretary = to_user(Auth::user())->secretaries()->find($id);
            if ($secretary) {
                $secretary->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'token' => '',
                    'password' => $request->password,
                    'social_id' => ''
                ]);
                return  response()->json($secretary, 200);
            } else {
                return response()->json('Not Found', 404);
            }
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (to_user(Auth::user())->hasPermissionTo('secretary-destroy')) {
            $secretary = User::with("roles")->whereHas("roles", function ($q) {
                $q->whereIn("name", ["secretary"]);
            })->where('doctor_id', to_user(Auth::user())->id)->find($id);
            if ($secretary) {
                $secretary->delete();
                return  response()->json();
            } else {
                return response()->json('Not Found', 404);
            }
        } else {
            return  response()->json('Not Allowed', 401);
        }
    }
}
