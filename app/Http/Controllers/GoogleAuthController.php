<?php

namespace App\Http\Controllers;

use App\Models\User;
use Google\Client;
use Illuminate\Http\Request;
use Google\Service\Calendar;
use Google\Service\Calendar\Event;
use GuzzleHttp\Client as c;
use Illuminate\Support\Facades\Auth;

class GoogleAuthController extends Controller
{
    public function redirectToGoogle()
    {
        $client = new Client();
        $client->setClientId(config('services.google.client_id'));
        $client->setClientSecret(config('services.google.client_secret'));
        $client->setRedirectUri(config('services.google.redirect_uri'));
        $client->setAccessType('offline');  $client->setApprovalPrompt ('force');
$client->addScope('https://www.googleapis.com/auth/calendar.events');
        $client->setScopes(
            [
                \Google\Service\Oauth2::USERINFO_PROFILE,
                \Google\Service\Oauth2::USERINFO_EMAIL,
                \Google\Service\Oauth2::OPENID,
                \Google\Service\Calendar::CALENDAR, // allows reading of google drive metadata
                \Google\Service\Calendar::CALENDAR_EVENTS,
            ]
        ); $client->setIncludeGrantedScopes(true);

        return redirect($client->createAuthUrl());
    }
    public function handleGoogleCallback(Request $request)
    {
        $client = new Client();
        $client->setClientId(config('services.google.client_id'));
        $client->setClientSecret(config('services.google.client_secret'));
        $client->setRedirectUri(config('services.google.redirect_uri'));
        $client->setApprovalPrompt ('force');
        $client->addScope('https://www.googleapis.com/auth/calendar.events');
                $client->setScopes(
                    [
                        \Google\Service\Oauth2::USERINFO_PROFILE,
                        \Google\Service\Oauth2::USERINFO_EMAIL,
                        \Google\Service\Oauth2::OPENID,
                        \Google\Service\Calendar::CALENDAR, // allows reading of google drive metadata
                        \Google\Service\Calendar::CALENDAR_EVENTS,
                    ]
                ); $client->setIncludeGrantedScopes(true);
        $token = $client->fetchAccessTokenWithAuthCode($request->get('code'));
        $refreshToken = $client->getRefreshToken();


        $client->setAccessToken($token);


        $service = new \Google\Service\Oauth2($client);
        $userFromGoogle = $service->userinfo->get();
        $user = User::where('email', $userFromGoogle->email)->first();
        if (!$user) {
            $user =  User::create([
                'name' => $userFromGoogle->name,
                'email' => $userFromGoogle->email,
                'token' => $refreshToken,
                'password' => $userFromGoogle->id,
                'social_id' => $userFromGoogle->id
            ]);
            $user->assignRole('doctor');

            $data = [
                'email' => $user->email,
                'password' => $user->social_id
            ];
            Auth::attempt($data);
            return redirect('/');
        } else {
            $data = [
                'email' => $user->email,
                'password' => $user->social_id
            ];
            if (Auth::attempt($data)) {

                to_user(Auth::user())->token=$refreshToken;
                to_user(Auth::user())->save();
                return redirect('/');
            } else {
                return redirect()->back(401);
            }
        }



    }
}
