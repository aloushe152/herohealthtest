<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'summary' => 'required',
            'description' => 'required',
            'start' =>[ 'required', 'date'],
            'end' => ['required', 'date']
        ];
    }
}
