<?php

use App\Http\Controllers\API\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\API\EventController;
use App\Http\Controllers\API\SecretaryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::resource('events', EventController::class)->middleware('auth:sanctum');
Route::resource('secretaries', SecretaryController::class)->middleware('auth:sanctum');
Route::post('login', [AuthController::class, 'login'])->name('auth.login');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
