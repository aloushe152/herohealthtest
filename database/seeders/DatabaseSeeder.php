<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $permissions = [
            'secretary-index',
            'secretary-store',
            'secretary-update',
            'secretary-destroy',
            'event-index',
            'event-store',
            'event-update',
            'event-destroy',
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create(['name' => $permission]);
        }


        $doctor = Role::create(['name' => 'doctor']);
        $doctor->syncPermissions(Permission::whereIn('name', [
            'secretary-index',
            'secretary-store',
            'secretary-update',
            'secretary-destroy',
            'event-index',
            'event-store',
            'event-update',
            'event-destroy',
        ])->get());


        $secretary = Role::create(['name' => 'secretary']);
        $secretary->givePermissionTo(Permission::whereIn('name', [
            'event-index',
        ])->get());
    }
}
